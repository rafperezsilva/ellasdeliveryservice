package com.ellas.ellasdeliveryservice;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ellas.ellasdeliveryservice.DeliveryModels.DeliveryAddressModel;
import com.ellas.ellasdeliveryservice.DeliveryModels.DeliveryDataModel;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    String TAG;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    Date currentTime = Calendar.getInstance().getTime();
     private DatabaseReference mDatabase;// ...
    private ValueEventListener postListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        currentTime.toString();



         writeNewUser();
         //readDeliveryData();
       // buttonMainLisener();
      //  setNotificationService();
    }

    private void writeNewUser () { //(DeliveryDataModel deliveryDataModel, DeliveryDataModel[] names) {
        mDatabase = FirebaseDatabase.getInstance().getReference();

         DeliveryDataModel[] names = {createDummyData()};
           List nameList2 = new ArrayList<DeliveryDataModel>(Arrays.asList(names));
        Log.w(TAG, String.valueOf(nameList2));

      String[] names2 = {"John","Tim","Sam","Ben"};
        List nameList = new ArrayList<String>(Arrays.asList(names2));



        mDatabase.child("delivery").setValue(nameList);

 //       mDatabase.setValue(delivery);
    }
   //public void  createDummyData(){
   public DeliveryDataModel   createDummyData(){
       // DeliveryGetDataModel dataArray = new DeliveryGetDataModel();

        DeliveryDataModel dataDelivery  = new DeliveryDataModel();
        dataDelivery.affiliate_id = "EDS1";
        dataDelivery.full_name = "Pepe Trueno";

        DeliveryAddressModel addressStart = new   DeliveryAddressModel();
        DeliveryAddressModel addressEnd = new   DeliveryAddressModel();
        addressStart.latitude = "123456.12";
        addressStart.longitude = "123456.12";
        addressStart.detail_address = "Cerca de la casa tuya";
        addressStart.reference_point = "Al lAdo del Kiosko azul";


        addressEnd.latitude = "123456.12";
        addressEnd.longitude = "123456.12";
        addressEnd.detail_address = "Calle topacio con esquina el perro";
        addressEnd.reference_point = "Frente a farmatodo";

        dataDelivery.start_point = addressStart;
        dataDelivery.end_point = addressEnd;
        return  dataDelivery;
    }

   private    void  readDeliveryData(){
        postListener = new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               // List<DeliveryDataModel> children = dataSnapshot.child("delivery").getValue(new GenericTypeIndicator<List<DeliveryDataModel>>(){});
                //children.add(createDummyData());
               // writeNewUser(createDummyData(), (DeliveryDataModel[]) children.toArray());


                // Get Post object and use the values to update the UI
                //List nameList = new ArrayList<>();
                  String[]  names =  dataSnapshot.child("delivery").getValue(String[].class);//child("delivery").getValue(String[].class);
                 //ArrayList<DeliveryDataModel> post = (DeliveryDataModel) dataSnapshot.child("delivery").getValue();
                //nameList = (List) dataSnapshot.child("delivery").getValue();
                  Log.w(TAG, String.valueOf(names ));
             //    Toast.makeText(getBaseContext(), children.toString() ,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
             //   Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
       mDatabase.addValueEventListener(postListener);
    }

    private  void buttonMainLisener(){
        Button deliveryButton = (Button) findViewById(R.id.delivery_button);
        deliveryButton.setOnClickListener(new View.OnClickListener() {
            // Write a message to the database

            @Override
            public void onClick(View view) {
                writeOnDataBase("delivery/valor2");
                readFromDataBase();
            }});

        Button mapButton = (Button) findViewById(R.id.map_button);
        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), EDSMapMain.class);
                view.getContext().startActivity(intent);
            }
        });
    }
    private void writeOnDataBase(String keyData){
        DatabaseReference myRef = database.getReference(keyData);
        //myRef =  database.getReference("valor");
        myRef.setValue("en java");
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }
    private  void  readFromDataBase(){
        Toast.makeText(getBaseContext(), "CLICK ON DELIVERY ICON",Toast.LENGTH_LONG).show();
        Log.d(TAG, "CLICK ON DELIVERY ICON");



    }
    private void setNotificationService() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {

                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                       // Toast.makeText(MainActivity.this, token ,Toast.LENGTH_LONG).show();
                        Log.d(TAG, token);
                        // Log and toast
                        @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String msg = getString(R.string.google_app_id, token);
                        Log.d(TAG, msg);
                        //Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }


}


