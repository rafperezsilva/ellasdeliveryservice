package com.ellas.ellasdeliveryservice.DeliveryModels;

import com.google.firebase.database.IgnoreExtraProperties;


@IgnoreExtraProperties
public class DeliveryDataModel  {

    public DeliveryAddressModel start_point;
    public DeliveryAddressModel end_point;

    public String full_name;
    public String affiliate_id;


    public DeliveryDataModel() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public DeliveryDataModel(String full_name,
                             String affiliate_id,
                             DeliveryAddressModel start_point,
                             DeliveryAddressModel end_point) {
        this.full_name = full_name;
        this.affiliate_id = affiliate_id;
        this.start_point = start_point;
        this.end_point = end_point;
    }

}

