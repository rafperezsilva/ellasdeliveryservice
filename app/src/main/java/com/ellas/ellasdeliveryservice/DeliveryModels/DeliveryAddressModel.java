package com.ellas.ellasdeliveryservice.DeliveryModels;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class DeliveryAddressModel {

    public String latitude;
    public String longitude;
    public String detail_address;
    public String reference_point;

    public DeliveryAddressModel() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public DeliveryAddressModel(String latitude, String longitude, String detail_address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.detail_address = detail_address;
    }
}
